# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core.validators import MinValueValidator


# Create your models here.
class Customer(models.Model):   
    name = models.CharField(max_length = 200)
    phone = models.IntegerField()   
    address = models.CharField(max_length = 200)  
    email= models.EmailField(max_length = 254) 
    block_status= models.BooleanField(default=False) 
    # class Meta:  
    #     db_table = "customer"  
    
    def __str__(self):
        return self.name
   


class Product(models.Model):
    name = models.CharField(max_length = 200)
    price = models.IntegerField()   
    quantity = models.PositiveIntegerField(default=5, validators=[MinValueValidator(0)])   
    # class Meta:  
    #     db_table = "product"   
    def __str__(self):
        return self.name


class Invoice(models.Model):
    shipping_address = models.CharField(max_length = 200)  
    created_at= models.DateTimeField('created_at')
    customer=models.ForeignKey(Customer,on_delete=models.CASCADE)
    product=models.ManyToManyField('Product')
    # class Meta:  
    #     db_table = "invoice"  
    def __str__(self):
        return self.name





