from django.conf.urls import  url
from django.contrib import admin
from invoice import views
# from .views import GeneratePDF



urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^customers$', views.customer_list, name='customer_list'),
    url(r'^customer/new$', views.customer_create, name='customer_create'),
    url(r'^customer/detail/(?P<pk>\d+)/$', views.customer_detail, name='customer_detail'),
    url(r'^products$', views.product_list, name='product_list'),
    url(r'^product/new$', views.product_create, name='product_create'),
    url(r'^product/detail/(?P<pk>\d+)/$', views.product_detail, name='product_detail'),
    url(r'^customer/invoice/(?P<id>\d+)/$', views.invoicebycustomerid, name='invoicebycustomerid'),
    url(r'^invoices', views.invoice_list, name = 'invoice_list'),
	url(r'^new$', views.invoice_create, name = 'invoice_create'),
	url(r'^detail/(?P<id>\d+)/$', views.invoice_detail, name = 'invoice_detail'),
    url(r'^pdf/(?P<id>\d+)/$', views.htmltopdf , name = 'htmltopdf'), 
    # url(r'^sendemail/(?P<id>\d+)/$', views.send_email , name = 'send_email'), 




]