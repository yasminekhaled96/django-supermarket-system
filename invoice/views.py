# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, redirect, get_object_or_404
from .models import Customer,Invoice,Product
from .forms import CustomerForm,InvoiceForm,ProductForm
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
#rendering html to pdf
from django.views.generic import View
from .utils import render_to_pdf 
from django.template.loader import get_template
#send email
from supermarketsystem.settings import EMAIL_HOST_USER
from django.core.mail import send_mail



@login_required(login_url='/')
def customer_list(request, template_name='customer/customer_list.html'):
    customers = Customer.objects.all()
    data = {}
    data['object_list'] = customers
    return render(request, template_name, data)

@login_required(login_url='/')
def customer_create(request, template_name='customer/customer_form.html'):
    form = CustomerForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('invoice:customer_list')
    return render(request, template_name, {'form': form})

@login_required(login_url='/')
def customer_detail(request, pk, template_name='customer/customer_detail.html'):
    customer= get_object_or_404(Customer, pk=pk)    
    return render(request, template_name, {'object':customer})
   

@login_required(login_url='/')
def product_list(request, template_name='product/product_list.html'):
    products = Product.objects.all()
    data = {}
    data['object_list'] = products
    return render(request, template_name, data)

@login_required(login_url='/')
def product_create(request, template_name='product/product_form.html'):
    form = ProductForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('invoice:product_list')
    return render(request, template_name, {'form': form})

@login_required(login_url='/')
def product_detail(request, pk, template_name='product/product_detail.html'):
    product= get_object_or_404(Product, pk=pk)    
    return render(request, template_name, {'object':product})


@login_required(login_url='/')
def invoice_list(request):
	invoices = Invoice.objects.all()
	return render(request, 'invoice/invoice_list.html', { 'invoices': invoices })


@login_required(login_url='/')
def invoice_create(request, template_name='invoice/invoice_form.html'):
    form = InvoiceForm(request.POST or None)
    if form.is_valid():
        form.save()
        subject = 'Purchase Confirmation'
        message = 'Your Invoice Data: ' + str(form.cleaned_data)
        recepient = 'yasmina.khaled96@gmail.com'
        # recepient = str(form.cleaned_data.get("customer"))
        send_mail(subject, message, EMAIL_HOST_USER, [recepient], fail_silently = False)
        return redirect('invoice:invoice_list')
    return render(request, template_name, {'form': form})

@login_required(login_url='/')
def invoice_detail(request, id):
	invoice = Invoice.objects.get(id = id)
	return render(request, 'invoice/invoice_detail.html', { 'invoice': invoice })
 

@login_required(login_url='/')
def invoicebycustomerid(request, id):
    customer = Customer.objects.get(id = id)
    invoices = Invoice.objects.filter(customer = customer)
    return render(request, 'invoice/invoice_list.html', { 'invoices': invoices })


@login_required(login_url='/')
def htmltopdf(request, id, *args, **kwargs):
        invoice = Invoice.objects.get(id = id)
        template = get_template('invoice/invoice_detail.html')
        html = template.render({'invoice': invoice })
        pdf = render_to_pdf('invoice/invoice_detail.html', {'invoice': invoice })
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "Invoice_%s.pdf" %("12341231")
            content = "inline; filename='%s'" %(filename)
            download = request.GET.get("download")
            if download:
                content = "attachment; filename='%s'" %(filename)
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not found")
